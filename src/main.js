const container = document.getElementById("peopleContainer");
const checkBtn = document.querySelector(".checkBtn")
const list = [];
let startIndex = 0;

const richestPeople = [
    'Jeff Bezos',
    'Bill Gates',
    'Warren Buffett',
    'Bernard Arnault',
    'Carlos Slim Helu',
    'Amancio Ortega',
    'Larry Ellison',
    'Mark Zuckerberg',
    'Michael Bloomberg',
    'Larry Page'
];

createList(richestPeople);

function createList(people) {
    people
      .map(person => ({ value: person, num: Math.random() }))
      .sort((a, b) => a.num - b.num)
      .map(obj => obj.value)
      .forEach((person, index) => {
        const li = document.createElement('li');
        li.setAttribute('data-index', index);
        li.innerHTML = `
          <span class="number">${index + 1}</span>
          <div class="draggable" draggable="true">
            <p class="person-name">${person}</p>
            <i class="fas fa-grip-lines"></i>
          </div>
          `;
        list.push(li);
        container.appendChild(li);
      });
    addEvents()
}

function dragStart() {
    startIndex = +this.closest("li").getAttribute('data-index');
}

function dragDrop() {
    const endIndex = +this.getAttribute("data-index");
    this.classList.remove("over");
    changePersons(startIndex, endIndex);
}

function changePersons(first, second) {
    const firstItem = list[first].querySelector(".draggable");
    const secondItem = list[second].querySelector(".draggable");
    list[first].append(secondItem);
    list[second].append(firstItem);

}

function dragEnter() {
    this.classList.add('over');
}

function dragOver(event) {
    event.preventDefault();
}

function dragLeave() {
    this.classList.remove('over');
}

function checkOrder(richestPeople, list) {
    list.forEach((person, index) => {
        const name = person.querySelector(".draggable").innerText.trim();
        if(name === richestPeople[index]) {
            person.classList.remove("wrong")
            person.classList.add("right")
        } else {
            person.classList.remove("right");
            person.classList.add("wrong")
        }
    })
}

function addEvents () {
    const draggables = document.querySelectorAll(".draggable");
    const draggableItems = container.querySelectorAll("li");

    draggables.forEach(draggable => {
        draggable.addEventListener("dragstart", dragStart);
    });

    draggableItems.forEach(draggableItem => {
        draggableItem.addEventListener('dragover', dragOver);
        draggableItem.addEventListener('drop', dragDrop);
        draggableItem.addEventListener('dragenter', dragEnter);
        draggableItem.addEventListener('dragleave', dragLeave);
      });
}

checkBtn.addEventListener("click", () => checkOrder(richestPeople, list));























